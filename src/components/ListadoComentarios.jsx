import { useEffect, useState } from 'react';

import { Comentario } from './Comentario';
import { Section } from './Section';

const URL_COMMENTS = 'https://jsonplaceholder.typicode.com/comments';
export function ListadoComentarios() {
  let [comentarios, setComentarios] = useState(null);

  useEffect(() => {
    fetch(URL_COMMENTS)
      .then((response) => response.json())
      .then((datos) => setComentarios(datos));
  }, []);

  return (
    <Section titulo='Comentarios'>
      <div className='container'>
        <div className='row'>
          {comentarios ? (
            comentarios.slice(0, 15).map((comentario) => {
              return (
                <Comentario
                  key={comentario.name}
                  nombre={comentario.name}
                  contenido={comentario.body}
                />
              );
            })
          ) : (
            <h1>Cargando comentarios...</h1>
          )}
        </div>
      </div>
    </Section>
  );
}
