import { Link } from 'react-router-dom';

export function Menu() {
  return (
    <div>
      <h1 className='text-center'>Hello World</h1>
      <ul className='nav '>
        <li className='mr-2'>
          <Link className='nav-link' to='/'>
            Usuarios
          </Link>
        </li>
        <li className='nav-item mr-2'>
          <Link className='nav-link' to='/fotos'>
            Fotos
          </Link>
        </li>
        <li className='nav-item mr-2'>
          <Link className='nav-link' to='/comentarios'>
            Comentarios
          </Link>
        </li>
      </ul>
    </div>
  );
}
