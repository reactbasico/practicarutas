import { useEffect, useState } from 'react';

import { Foto } from './Foto';
import { Section } from './Section';

const URL_FOTOS = 'https://jsonplaceholder.typicode.com/photos';

export function ListadoFotos() {
  let [fotos, setFotos] = useState([]);

  useEffect(() => {
    fetch(URL_FOTOS)
      .then((response) => response.json())
      .then((datos) => setFotos(datos));
  }, []);

  return (
    <Section titulo='Fotos'>
      <div className='container'>
        <div className='row'>
          {fotos.slice(0, 15).map((foto) => {
            return (
              <Foto
                key={foto.title}
                titulo={foto.title}
                urlImagen={foto.thumbnailUrl}
              />
            );
          })}
        </div>
      </div>
    </Section>
  );
}
