import { useEffect, useState } from 'react';

import { Section } from './Section';
import { Usuario } from './Usuario';

const URL_USERS = 'https://jsonplaceholder.typicode.com/users';

export function ListadoUsuarios() {
  let [usuarios, setUsuarios] = useState(null);

  useEffect(() => {
    fetch(URL_USERS)
      .then((response) => response.json())
      .then((datos) => setUsuarios(datos));
  }, []);

  return (
    <Section titulo='Usuarios'>
      <div className='container'>
        <div className='row'>
          {usuarios ? (
            usuarios.map((usuario) => {
              return (
                <Usuario
                  key={usuario.username}
                  nombre={usuario.name}
                  apodo={usuario.username}
                  correo={usuario.email}
                />
              );
            })
          ) : (
            <h1>Cargando usuarios...</h1>
          )}
        </div>
      </div>
    </Section>
  );
}
