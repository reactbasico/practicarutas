import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

import { ListadoComentarios } from './components/ListadoComentarios';
import { ListadoFotos } from './components/ListadoFotos';
import { ListadoUsuarios } from './components/ListadoUsuarios';
import { Menu } from './components/Menu';

function App() {
  return (
    <div className='App'>
      <Router>
        <Menu />
        <Routes>
          <Route path='/comentarios' element={<ListadoComentarios />} />

          <Route path='/fotos' element={<ListadoFotos />} />

          <Route path='/' element={<ListadoUsuarios />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
